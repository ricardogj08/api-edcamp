<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', ['as' => 'home', 'uses' => 'HomeController@index']);

$router->post('/v1/signup', ['as' => 'signup', 'uses' => 'UserController@store']);

/**
 * Autenticación de usuarios.
 */
$router->group(['prefix' => '/v1/auth', 'as' => 'auth'], function () use ($router) {
  /**
   * Coincide con la ruta /v1/auth/login y el nombre 'auth.login'.
   */
  $router->post('/login',   ['as' => 'login',   'uses' => 'AuthController@login']);
  $router->post('/logout',  ['as' => 'logout',  'uses' => 'AuthController@logout']);
  $router->post('/refresh', ['as' => 'refresh', 'uses' => 'AuthController@refresh']);
  $router->post('/me',      ['as' => 'me',      'uses' => 'AuthController@me']);
});

$router->group(['middleware' => 'auth:api'], function () use ($router) {
  $router->group(['prefix' => '/v1/users', 'as' => 'users'], function () use ($router) {
    $router->get('/',        ['as' => 'index',   'uses' => 'UserController@index']);
    $router->get('/{id}',    ['as' => 'show',    'uses' => 'UserController@show']);
    $router->put('/{id}',    ['as' => 'update',  'uses' => 'UserController@update']);
    $router->delete('/{id}', ['as' => 'destroy', 'uses' => 'UserController@destroy']);
  });

  $router->group(['prefix' => '/v1/companies', 'as' => 'companies'], function () use ($router) {
    $router->get('/',        ['as' => 'index',   'uses' => 'CompanyController@index']);
    $router->post('/',       ['as' => 'store',   'uses' => 'CompanyController@store']);
    $router->get('/{id}',    ['as' => 'show',    'uses' => 'CompanyController@show']);
    $router->put('/{id}',    ['as' => 'update',  'uses' => 'CompanyController@update']);
    $router->delete('/{id}', ['as' => 'destroy', 'uses' => 'CompanyController@destroy']);
  });

  $router->group(['prefix' => '/v1/prices', 'as' => 'prices'], function () use ($router) {
    $router->get('/',        ['as' => 'index',   'uses' => 'PriceController@index']);
    $router->post('/',       ['as' => 'store',   'uses' => 'PriceController@store']);
    $router->get('/{id}',    ['as' => 'show',    'uses' => 'PriceController@show']);
    $router->put('/{id}',    ['as' => 'update',  'uses' => 'PriceController@update']);
    $router->delete('/{id}', ['as' => 'destroy', 'uses' => 'PriceController@destroy']);
  });

  $router->group(['prefix' => '/v1/students', 'as' => 'students'], function () use ($router) {
    $router->get('/',        ['as' => 'index',   'uses' => 'StudentController@index']);
    $router->post('/',       ['as' => 'store',   'uses' => 'StudentController@store']);
    $router->get('/{id}',    ['as' => 'show',    'uses' => 'StudentController@show']);
    $router->put('/{id}',    ['as' => 'update',  'uses' => 'StudentController@update']);
    $router->delete('/{id}', ['as' => 'destroy', 'uses' => 'StudentController@destroy']);
  });

  $router->group(['prefix' => '/v1/subscriptions', 'as' => 'subscriptions'], function () use ($router) {
    $router->get('/',        ['as' => 'index',   'uses' => 'SubscriptionController@index']);
    $router->post('/',       ['as' => 'store',   'uses' => 'SubscriptionController@store']);
    $router->get('/{id}',    ['as' => 'show',    'uses' => 'SubscriptionController@show']);
    $router->put('/{id}',    ['as' => 'update',  'uses' => 'SubscriptionController@update']);
    $router->delete('/{id}', ['as' => 'destroy', 'uses' => 'SubscriptionController@destroy']);
  });
});

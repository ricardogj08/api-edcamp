<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Student extends Model {
  public $incrementing = false;
  protected $keyType  = 'string';
  protected $fillable = [
    'id', 'company_id', 'email', 'name', 'lastname',
    'phone', 'payment_status', 'presence'
  ];
  protected $attributes = [
    'presence' => false,
    'payment_status' => 'Pendiente'
  ];

  /**
   * Establece la relación Students->Company
   */
  public function company() {
    return $this->belongsTo(Company::class);
  }
}

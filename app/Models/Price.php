<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Price extends Model {
  public $incrementing = false;
  protected $keyType  = 'string';
  protected $fillable = ['id', 'type', 'cost', 'active'];
  protected $attributes = ['active' => false];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Subscription extends Model {
  public $incrementing = false;
  protected $keyType  = 'string';
  protected $fillable = ['id', 'student_id', 'price_id', 'observation'];

  /**
   * Establece la relación Subscriptions->Student
   */
  public function student() {
    return $this->belongsTo(Student::class);
  }

  /**
   * Establece la relación Subscriptions->Price
   */
  public function price() {
    return $this->belongsTo(Price::class);
  }
}

<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\StudentResource;
use App\Http\Resources\PriceResource;

class SubscriptionResource extends JsonResource {
  /**
   * Transform the resource into an array.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return array
   */
  public function toArray($request) {
    return [
      'id' => $this->id,
      'student' => new StudentResource($this->whenLoaded('student')),
      'price' => new PriceResource($this->whenLoaded('price')),
      'observation' => $this->observation,
      'created_at'  => $this->created_at,
      'updated_at'  => $this->updated_at,
    ];
  }
}

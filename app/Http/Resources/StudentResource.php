<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\CompanyResource;

class StudentResource extends JsonResource {
  /**
   * Transform the resource into an array.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return array
   */
  public function toArray($request) {
    return [
      'id'  => $this->id,
      'email' => $this->email,
      'name'  => $this->name,
      'lastname' => $this->lastname,
      'company'  => new CompanyResource($this->whenLoaded('company')),
      'phone' => $this->phone,
      'payment_status' => $this->payment_status,
      'presence'   => $this->presence,
      'created_at' => $this->created_at,
      'updated_at' => $this->updated_at,
    ];
  }
}

<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Student;
use App\Http\Resources\StudentResource;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Illuminate\Validation\Rules\Enum;
use App\Enums\PaymentStatus;

class StudentController extends Controller {
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct() {
    //
  }

  public function index(Request $request) {
    /**
     * Valida los parámetros de consulta de la ruta.
     */
    $query = $this->validate($request, [
      'sortOrder' => ['bail', 'nullable', 'string', Rule::in(['asc', 'desc'])],
    ]);

    $sortOrder = Arr::get($query, 'sortOrder', 'asc');

    return StudentResource::collection(Student::with('company')
    ->orderBy('name', $sortOrder)
    ->orderBy('lastname', $sortOrder)
    ->get())
    ->additional([
      'message' => [
        'type' => 'success',
        'code' => Response::HTTP_OK,
        'description' => 'Lista de estudiantes.',
    ]]);
  }

  public function store(Request $request) {
    /**
     * Valida los campos de la petición.
     */
    $input = $this->validate($request, [
      'company_id' => 'bail|required|uuid|exists:App\Models\Company,id',
      'email' => 'bail|required|email|unique:App\Models\Student',
      'name'  => 'bail|required|string|max:64',
      'lastname' => 'bail|required|string|max:64',
      'phone' => 'bail|required|string|min:10|max:10',
      'payment_status' => ['bail', 'nullable', 'string', new Enum(PaymentStatus::class)],
      'presence' => 'bail|required|boolean',
    ]);

    $input['id'] = Str::uuid();

    $id = Student::create($input)->id;

    return (new StudentResource(Student::with('company')
    ->findOrFail($id)))
    ->additional([
      'message' => [
        'type' => 'success',
        'code' => Response::HTTP_CREATED,
        'description' => 'El estudiante se ha registrado correctamente.',
    ]])
    ->response()
    ->setStatusCode(Response::HTTP_CREATED);
  }

  public function show(Request $request, $id) {
    /**
     * Valida los parámetros de la ruta.
     */
    Validator::make(['id' => $id], [
      'id' => 'bail|required|uuid',
    ])->validated();

    return (new StudentResource(Student::with('company')
    ->findOrFail($id)))
    ->additional([
      'message' => [
        'type' => 'success',
        'code' => Response::HTTP_OK,
        'description' => 'Datos del estudiante.',
    ]]);
  }

  public function update(Request $request, $id) {
    /**
     * Valida los parámetros de la ruta.
     */
    Validator::make(['id' => $id], [
      'id' => 'bail|required|uuid',
    ])->validated();

    /**
     * Valida los campos de la petición.
     */
    $input = $this->validate($request, [
      'company_id' => 'bail|nullable|uuid|exists:App\Models\Company,id',
      'email' => 'bail|nullable|email|unique:App\Models\Student',
      'name'  => 'bail|nullable|string|max:64',
      'lastname' => 'bail|nullable|string|max:64',
      'phone' => 'bail|nullable|string|min:10|max:10',
      'payment_status' => ['bail', 'nullable', 'string', new Enum(PaymentStatus::class)],
      'presence' => 'bail|nullable|boolean',
    ]);

    $student = Student::findOrFail($id);

    $student->update($input);

    return (new StudentResource(Student::with('company')
    ->find($id)))
    ->additional([
      'message' => [
        'type' => 'success',
        'code' => Response::HTTP_OK,
        'description' => 'Los datos del estudiante se han modificado correctamente.',
    ]]);
  }

  public function destroy(Request $request, $id) {
    /**
     * Valida los parámetros de la ruta.
     */
    Validator::make(['id' => $id], [
      'id' => 'bail|required|uuid',
    ])->validated();

    $student = Student::with('company')->findOrFail($id);

    $student->delete();

    return (new StudentResource($student))
    ->additional([
      'message' => [
        'type' => 'success',
        'code' => Response::HTTP_OK,
        'description' => 'El estudiante se ha eliminado correctamente.',
    ]]);
  }
}

<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Response;

class HomeController extends Controller {
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct() {
    //
  }

  public function index() {
    return [
      'data' => [
        'app' => 'api-edcamp',
        'description' => 'Una API RESTful de inscripción de alumnos al EDcamp de EDteam.',
        'license' => [
          'name' => 'CC0 1.0 Universal',
          'link' => 'https://creativecommons.org/publicdomain/mark/1.0/deed.es',
        ],
        'repo' => 'https://git.disroot.org/ricardogj08/api-edcamp',
        'author' => [
          'name' => 'Ricardo García Jiménez',
          'email' => 'ricardogj08@riseup.net',
          'social' => 'https://cawfee.club/ricardogj08',
          'repos' => [
            'https://github.com/ricardogj08',
            'https://git.disroot.org/ricardogj08',
          ],
        ],
      ],
      'message' => [
        'type' => 'success',
        'code' => Response::HTTP_OK,
        'description' => 'Info.'
      ]
    ];
  }
}

<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Price;
use App\Http\Resources\PriceResource;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class PriceController extends Controller {
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct() {
    //
  }

  public function index(Request $request) {
    /**
     * Valida los parámetros de consulta de la ruta.
     */
    $query = $this->validate($request, [
      'sortOrder' => ['bail', 'nullable', 'string', Rule::in(['asc', 'desc'])],
    ]);

    $sortOrder = Arr::get($query, 'sortOrder', 'asc');

    return PriceResource::collection(Price::orderBy('type', $sortOrder)
    ->orderBy('cost', $sortOrder)
    ->get())
    ->additional([
      'message' => [
        'type' => 'success',
        'code' => Response::HTTP_OK,
        'description' => 'Lista de precios.',
    ]]);
  }

  public function store(Request $request) {
    /**
     * Valida los campos de la petición.
     */
    $input = $this->validate($request, [
      'type' => 'bail|required|string|max:24|unique:App\Models\Price',
      'cost' => 'bail|required|integer|min:0',
      'active' => 'bail|nullable|boolean',
    ]);

    $input['id'] = Str::uuid();

    $id = Price::create($input)->id;

    return (new PriceResource(Price::findOrFail($id)))
    ->additional([
      'message' => [
        'type' => 'success',
        'code' => Response::HTTP_CREATED,
        'description' => 'El precio se ha registrado correctamente.',
    ]])
    ->response()
    ->setStatusCode(Response::HTTP_CREATED);
  }

  public function show(Request $request, $id) {
    /**
     * Valida los parámetros de la ruta.
     */
    Validator::make(['id' => $id], [
      'id' => 'bail|required|uuid',
    ])->validated();

    return (new PriceResource(Price::findOrFail($id)))
    ->additional([
      'message' => [
        'type' => 'success',
        'code' => Response::HTTP_OK,
        'description' => 'Datos del precio.',
    ]]);
  }

  public function update(Request $request, $id) {
    /**
     * Valida los parámetros de la ruta.
     */
    Validator::make(['id' => $id], [
      'id' => 'bail|required|uuid',
    ])->validated();

    /**
     * Valida los campos de la petición.
     */
    $input = $this->validate($request, [
      'type' => 'bail|nullable|string|max:24|unique:App\Models\Price',
      'cost' => 'bail|nullable|integer|min:0',
      'active' => 'bail|nullable|boolean',
    ]);

    $price = Price::findOrFail($id);

    $price->update($input);

    return (new PriceResource(Price::find($id)))
    ->additional([
      'message' => [
        'type' => 'success',
        'code' => Response::HTTP_OK,
        'description' => 'Los datos del precio se han modificado correctamente.',
    ]]);
  }

  public function destroy(Request $request, $id) {
    /**
     * Valida los parámetros de la ruta.
     */
    Validator::make(['id' => $id], [
      'id' => 'bail|required|uuid',
    ])->validated();

    $price = Price::findOrFail($id);

    $price->delete();

    return (new PriceResource($price))
    ->additional([
      'message' => [
        'type' => 'success',
        'code' => Response::HTTP_OK,
        'description' => 'El precio se ha eliminado correctamente.',
    ]]);
  }
}

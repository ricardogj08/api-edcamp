<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Subscription;
use App\Http\Resources\SubscriptionResource;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class SubscriptionController extends Controller {
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct() {
    //
  }

  public function index(Request $request) {
    /**
     * Valida los parámetros de consulta de la ruta.
     */
    $query = $this->validate($request, [
      'sortOrder' => ['bail', 'nullable', 'string', Rule::in(['asc', 'desc'])],
    ]);

    $sortOrder = Arr::get($query, 'sortOrder', 'asc');

    return SubscriptionResource::collection(Subscription::with(['student', 'price'])
    ->orderBy('updated_at', $sortOrder)
    ->get())
    ->additional([
      'message' => [
        'type' => 'success',
        'code' => Response::HTTP_OK,
        'description' => 'Lista de suscripciones.',
    ]]);
  }

  public function store(Request $request) {
    /**
     * Valida los campos de la petición.
     */
    $input = $this->validate($request, [
      'student_id' => 'bail|required|uuid|exists:App\Models\Student,id',
      'price_id' => 'bail|required|uuid|exists:App\Models\Price,id',
      'observation' => 'bail|nullable|string|max:256',
    ]);

    $input['id'] = Str::uuid();

    $id = Subscription::create($input)->id;

    return (new SubscriptionResource(Subscription::with(['student', 'price'])
    ->findOrFail($id)))
    ->additional([
      'message' => [
        'type' => 'success',
        'code' => Response::HTTP_CREATED,
        'description' => 'La suscripción se ha registrado correctamente.',
    ]])
    ->response()
    ->setStatusCode(Response::HTTP_CREATED);
  }

  public function show(Request $request, $id) {
    /**
     * Valida los parámetros de la ruta.
     */
    Validator::make(['id' => $id], [
      'id' => 'bail|required|uuid',
    ])->validated();

    return (new SubscriptionResource(Subscription::with(['student', 'price'])
    ->findOrFail($id)))
    ->additional([
      'message' => [
        'type' => 'success',
        'code' => Response::HTTP_OK,
        'description' => 'Datos de la suscripción.',
    ]]);
  }

  public function update(Request $request, $id) {
    /**
     * Valida los parámetros de la ruta.
     */
    Validator::make(['id' => $id], [
      'id' => 'bail|required|uuid',
    ])->validated();

    /**
     * Valida los campos de la petición.
     */
    $input = $this->validate($request, [
      'student_id' => 'bail|nullable|uuid|exists:App\Models\Student,id',
      'price_id' => 'bail|nullable|uuid|exists:App\Models\Price,id',
      'observation' => 'bail|nullable|string|max:256',
    ]);

    $subscription = Subscription::findOrFail($id);

    $subscription->update($input);

    return (new SubscriptionResource(Subscription::with(['student', 'price'])
    ->find($id)))
    ->additional([
      'message' => [
        'type' => 'success',
        'code' => Response::HTTP_OK,
        'description' => 'Los datos de la suscripción se han modificado correctamente.',
    ]]);
  }

  public function destroy(Request $request, $id) {
    /**
     * Valida los parámetros de la ruta.
     */
    Validator::make(['id' => $id], [
      'id' => 'bail|required|uuid',
    ])->validated();

    $subscription = Subscription::with(['student', 'price'])->findOrFail($id);

    $subscription->delete();

    return (new SubscriptionResource($subscription))
    ->additional([
      'message' => [
        'type' => 'success',
        'code' => Response::HTTP_OK,
        'description' => 'La suscripción se ha eliminado correctamente.',
    ]]);
  }
}

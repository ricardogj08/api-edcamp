<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Http\Resources\UserResource;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Illuminate\Validation\Rules\Password;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller {
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct() {
    $this->middleware('auth:api', ['except' => ['store']]);
  }

  public function index(Request $request) {
    /**
     * Valida los parámetros de consulta de la ruta.
     */
    $query = $this->validate($request, [
      'sortOrder' => ['bail', 'nullable', 'string', Rule::in(['asc', 'desc'])],
    ]);

    $sortOrder = Arr::get($query, 'sortOrder', 'asc');

    return UserResource::collection(User::orderBy('name', $sortOrder)
    ->get())
    ->additional([
      'message' => [
        'type' => 'success',
        'code' => Response::HTTP_OK,
        'description' => 'Lista de usuarios.',
    ]]);
  }

  public function store(Request $request) {
    /**
     * Valida los campos de la petición.
     */
    $input = $this->validate($request, [
      'name'  => 'bail|required|string|max:256',
      'email' => 'bail|required|email|max:256',
      'password' => ['bail', 'required', Password::defaults(), 'confirmed'],
    ]);

    $input['id'] = Str::uuid();
    $input['password'] = Hash::make($input['password']);

    $id = User::create($input)->id;

    return (new UserResource(User::findOrFail($id)))
    ->additional([
      'message' => [
        'type' => 'success',
        'code' => Response::HTTP_CREATED,
        'description' => 'El usuario se ha registrado correctamente.',
    ]])
    ->response()
    ->setStatusCode(Response::HTTP_CREATED);
  }

  public function show(Request $request, $id) {
    /**
     * Valida los parámetros de la ruta.
     */
    Validator::make(['id' => $id], [
      'id' => 'bail|required|uuid',
    ])->validated();

    return (new UserResource(User::findOrFail($id)))
    ->additional([
      'message' => [
        'type' => 'success',
        'code' => Response::HTTP_OK,
        'description' => 'Datos del usuario.',
    ]]);
  }

  public function update(Request $request, $id) {
    /**
     * Valida los parámetros de la ruta.
     */
    Validator::make(['id' => $id], [
      'id' => 'bail|required|uuid',
    ])->validated();

    /**
     * Valida los campos de la petición.
     */
    $input = $this->validate($request, [
      'name'  => 'bail|nullable|string|max:256',
      'email' => 'bail|nullable|email|max:256|unique:App\Models\User',
      'password' => ['bail', 'nullable', Password::defaults(), 'confirmed'],
    ]);

    if (Arr::exists($input, 'password'))
      $input['password'] = Hash::make($input['password']);

    $user = User::findOrFail($id);

    $user->update($input);

    return (new UserResource(User::find($id)))
    ->additional([
      'message' => [
        'type' => 'success',
        'code' => Response::HTTP_OK,
        'description' => 'Los datos del usuario se han modificado correctamente.',
    ]]);
  }

  public function destroy(Request $request, $id) {
    /**
     * Valida los parámetros de la ruta.
     */
    Validator::make(['id' => $id], [
      'id' => 'bail|required|uuid',
    ])->validated();

    $user = User::findOrFail($id);

    $user->delete();

    return (new UserResource($company))
    ->additional([
      'message' => [
        'type' => 'success',
        'code' => Response::HTTP_OK,
        'description' => 'El usuario se ha eliminado correctamente.',
    ]]);
  }
}

<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Company;
use App\Http\Resources\CompanyResource;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class CompanyController extends Controller {
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct() {
    //
  }

  public function index(Request $request) {
    /**
     * Valida los parámetros de consulta de la ruta.
     */
    $query = $this->validate($request, [
      'sortOrder' => ['bail', 'nullable', 'string', Rule::in(['asc', 'desc'])],
    ]);

    $sortOrder = Arr::get($query, 'sortOrder', 'asc');

    return CompanyResource::collection(Company::orderBy('name', $sortOrder)
    ->get())
    ->additional([
      'message' => [
        'type' => 'success',
        'code' => Response::HTTP_OK,
        'description' => 'Lista de compañías.',
    ]]);
  }

  public function store(Request $request) {
    /**
     * Valida los campos de la petición.
     */
    $input = $this->validate($request, [
      'name'  => 'bail|required|string|max:256|unique:App\Models\Company',
      'phone' => 'bail|required|string|min:10|max:10',
      'description' => 'bail|required|string|max:256',
    ]);

    $input['id'] = Str::uuid();

    $id = Company::create($input)->id;

    return (new CompanyResource(Company::findOrFail($id)))
    ->additional([
      'message' => [
        'type' => 'success',
        'code' => Response::HTTP_CREATED,
        'description' => 'La compañía se ha registrado correctamente.',
    ]])
    ->response()
    ->setStatusCode(Response::HTTP_CREATED);
  }

  public function show(Request $request, $id) {
    /**
     * Valida los parámetros de la ruta.
     */
    Validator::make(['id' => $id], [
      'id' => 'bail|required|uuid',
    ])->validated();

    return (new CompanyResource(Company::findOrFail($id)))
    ->additional([
      'message' => [
        'type' => 'success',
        'code' => Response::HTTP_OK,
        'description' => 'Datos de la compañía.',
    ]]);
  }

  public function update(Request $request, $id) {
    /**
     * Valida los parámetros de la ruta.
     */
    Validator::make(['id' => $id], [
      'id' => 'bail|required|uuid',
    ])->validated();

    /**
     * Valida los campos de la petición.
     */
    $input = $this->validate($request, [
      'name'  => 'bail|nullable|string|max:256|unique:App\Models\Company',
      'phone' => 'bail|nullable|string|min:10|max:10',
      'description' => 'bail|nullable|string|max:256',
    ]);

    $company = Company::findOrFail($id);

    $company->update($input);

    return (new CompanyResource(Company::find($id)))
    ->additional([
      'message' => [
        'type' => 'success',
        'code' => Response::HTTP_OK,
        'description' => 'Los datos de la compañía se han modificado correctamente.',
    ]]);
  }

  public function destroy(Request $request, $id) {
    /**
     * Valida los parámetros de la ruta.
     */
    Validator::make(['id' => $id], [
      'id' => 'bail|required|uuid',
    ])->validated();

    $company = Company::findOrFail($id);

    $company->delete();

    return (new CompanyResource($company))
    ->additional([
      'message' => [
        'type' => 'success',
        'code' => Response::HTTP_OK,
        'description' => 'La compañía se ha eliminado correctamente.',
    ]]);
  }
}

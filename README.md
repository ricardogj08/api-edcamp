# api-edcamp

Una API RESTful de inscripción de alumnos al EDcamp de EDteam.

Demo:

* <https://api-edcamp.herokuapp.com/>

## Dependencias

* [Lumen 9.x - Un micro framework de PHP por Laravel.](https://lumen.laravel.com/docs/9.x)
* [MySQL](https://www.mysql.com/) o [MariaDB](https://mariadb.com/) - Un sistema gestor de bases de datos relacionales.

Instala todas las dependencias del proyecto con [composer](https://getcomposer.org/):

```shell
$ cd api-edcamp
$ composer install
```

Copia el archivo `.env.example` (contiene todas las opciones de configuración del proyecto):

```shell
$ cp .env.example .env
```

## Configuración de la base de datos

Crea una base de datos para el proyecto:

```sql
CREATE DATABASE IF NOT EXISTS edcamp
  CHARACTER SET = 'utf8mb4'
  COLLATE       = 'utf8mb4_spanish_ci';
```

Construye todas las tablas de la base de datos:

```shell
$ php artisan migrate
```

## Autenticación

Genera los certificados para firmar e identificar los tokens:

```shell
$ php artisan jwt:secret
$ php artisan jwt:generate-certs
$ php -S localhost:8000 -t public/
```

> Utiliza [jwt-auth](https://github.com/PHP-Open-Source-Saver/jwt-auth).

## Licencia

[Licencia Creative Commons de Dominio Público 1.0](https://creativecommons.org/publicdomain/mark/1.0/deed.es)

Este programa (api-edcamp, por [Ricardo García Jiménez](https://git.disroot.org/ricardogj08/api-edcamp)) es libre de restricciones conocidas de derechos de autor.

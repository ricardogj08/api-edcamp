<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
  /**
   * Ejecuta la migración de la tabla companies.
   *
   * @return void
   */
  public function up() {
    Schema::create('companies', function (Blueprint $table) {
      $table->uuid('id')
            ->primary();

      $table->string('name', 256)
            ->unique();

      $table->string('phone', 10);

      $table->string('description', 256);

      $table->dateTime('created_at')
            ->useCurrent();

      $table->dateTime('updated_at')
            ->useCurrent()
            ->useCurrentOnUpdate();
    });
  }

  /**
   * Invierte la migración.
   *
   * @return void
   */
  public function down() {
    Schema::dropIfExists('companies');
  }
};

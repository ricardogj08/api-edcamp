<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
  /**
   * Ejecuta la migración de la tabla subscriptions.
   *
   * @return void
   */
  public function up() {
    Schema::create('subscriptions', function (Blueprint $table) {
      $table->uuid('id')
            ->primary();

      $table->foreignUuid('student_id')
            ->constrained()
            ->restrictOnUpdate()
            ->restrictOnDelete();

      $table->foreignUuid('price_id')
            ->constrained()
            ->restrictOnUpdate()
            ->restrictOnDelete();

      $table->string('observation', 256)
            ->nullable();

      $table->dateTime('created_at')
            ->useCurrent();

      $table->dateTime('updated_at')
            ->useCurrent()
            ->useCurrentOnUpdate();
    });
  }

  /**
   * Invierte la migración.
   *
   * @return void
   */
  public function down() {
    Schema::dropIfExists('subscriptions');
  }
};

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
  /**
   * Ejecuta la migración de la tabla prices.
   *
   * @return void
   */
  public function up() {
    Schema::create('prices', function (Blueprint $table) {
      $table->uuid('id')
            ->primary();

      $table->string('type', 24)
            ->unique();

      $table->float('cost', 6, 2)
            ->unsigned();

      $table->boolean('active')
            ->default(false);

      $table->dateTime('created_at')
            ->useCurrent();

      $table->dateTime('updated_at')
            ->useCurrent()
            ->useCurrentOnUpdate();
    });
  }

  /**
   * Invierte la migración.
   *
   * @return void
   */
  public function down() {
    Schema::dropIfExists('prices');
  }
};

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
  /**
   * Ejecuta la migración de la tabla students.
   *
   * @return void
   */
  public function up() {
    Schema::create('students', function (Blueprint $table) {
      $table->uuid('id')
            ->primary();

      $table->foreignUuid('company_id')
            ->constrained()
            ->restrictOnUpdate()
            ->restrictOnDelete();

      $table->string('email', 256)
            ->unique();

      $table->string('name', 64);

      $table->string('lastname', 64);

      $table->string('phone', 10);

      $table->enum('payment_status', ['Pagado', 'Pendiente'])
            ->default('Pendiente');

      $table->boolean('presence')
            ->default(false);

      $table->dateTime('created_at')
            ->useCurrent();

      $table->dateTime('updated_at')
            ->useCurrent()
            ->useCurrentOnUpdate();
    });
  }

  /**
   * Invierte la migración.
   *
   * @return void
   */
  public function down() {
    Schema::dropIfExists('students');
  }
};
